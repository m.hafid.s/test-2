import { setListener, initialize } from './firebase';
export const initApi = () => initialize();

export const getMessages = (updaterFn) => setListener('messages', updaterFn);